import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import Data_Binding from "./Data_Biding/Data_Binding";
import Event_Binding from "./Event_Binding/Event_Binding";
import Demo_State from "./Demo_State/Demo_State";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import Demo_Prop from "./Demo_Prop/Demo_Prop";
import UserInfor_Props from "./Demo_Prop/UserInfor_Props";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";

function App() {
  return (
    <div className="App">
      {/* <DemoClass></DemoClass> */}

      {/* <DemoFunction /> */}
      {/* <Ex_Layout /> */}
      {/* <Data_Binding /> */}
      {/* <Event_Binding /> */}
      {/* <Conditional_Rendering /> */}
      {/* <Demo_State /> */}
      {/* <RenderWithMap /> */}
      {/* <Demo_Prop /> */}
      <Ex_ShoeShop />
    </div>
  );
}

export default App;
