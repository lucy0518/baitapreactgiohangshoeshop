import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { name, image } = this.props.detail;

    return (
      <div className="card col-3 ">
        <img className="card-img-top" src={image} alt />
        <div className="card-body">
          <h5 className="card-title"> {name}</h5>

          <p className="card-text">
            Some quick....Lorem ipsum dolor sit amet consectetur adipisicing
            elit. Odio itaque, vel voluptas minima dignissimos vitae tempore
            numquam earum rerum, nesciunt dolore a harum, alias sint excepturi
            enim error blanditiis vero.
          </p>

          <button
            onClick={() => {
              this.props.handleAddToCart(this.props.detail);
            }}
            className="btn btn-secondary"
          >
            {" "}
            Add to cart{" "}
          </button>
          <button
            onClick={() => {
              this.props.handleViewDetail(this.props.detail.id);
            }}
            className="btn btn-warning"
          >
            {" "}
            Xem chi tiết{" "}
          </button>
        </div>
      </div>
    );
  }
}
