import React, { Component } from "react";
import { dataShoe } from "./DataShoe";
import DetailShoe from "./DetailShoe";
import GioHang from "./GioHang";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detailShoe: dataShoe[0],
    gioHang: [],
  };

  handleXemChiTiet = (idshoe) => {
    //c1
    let index = this.state.shoeArr.findIndex((item) => {
      return item.id == idshoe;
    });

    index !== -1 &&
      this.setState({
        detailShoe: this.state.shoeArr[index],
      });
  };

  handleAddToCart = (shoe) => {
    // lam soLuong
    let cloneGioHang = [...this.state.gioHang];

    let index = this.state.gioHang.findIndex((item) => {
      return item.id == shoe.id;
    });

    //truong hop 1 : sp chưa có trong giỏ hàng
    if (index == -1) {
      let spGioHang = { ...shoe, soLuong: 1 };
      console.log("spGioHang: ", spGioHang);
      cloneGioHang.push(spGioHang);
    } else {
      // truong hop 2 : sp đã có trong giỏ hàng
      // let sp = this.state.gioHang[index];
      // tăng sôs lượng lên 1 đơn vị
      //sp.soLuong++
      // cloneGioHang[index] = sp;
      cloneGioHang[index].soLuong++;
    }
    this.setState(
      {
        gioHang: cloneGioHang,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  render() {
    console.log(this.state.gioHang.length);
    return (
      <div>
        <GioHang gioHang={this.state.gioHang} />
        <ListShoe
          data={this.state.shoeArr}
          handleXemChiTiet={this.handleXemChiTiet}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}
