import React, { Component } from "react";

export default class GioHang extends Component {
  renderTbody = () => {
    if (this.props.gioHang.length == 0) {
      return <p className="mt-5"></p>;
    }
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            {""}
            <img
              src={item.image}
              style={{
                width: 80,
              }}
              alt=""
            />
          </td>
          <button className="btn btn-primary"> - </button>
          <span className="mx-3"> {item.soLuong} </span>
          <button className="btn btn-success"> + </button>

          <button className="btn btn-danger"> Xóa </button>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <table className="table text-left">
          <thead>
            <tr>
              <th> Tên </th>
              <th> Giá </th>
              <th> Hình Ảnh </th>
              <th>Số Lượng </th>
            </tr>
          </thead>
          <tbody> {this.renderTbody()}</tbody>
        </table>
        {this.props.gioHang.length == 0 && (
          <p className="mt-5 text-center"> Chưa có sản phẩm trong giỏ hàng </p>
        )}
      </div>
    );
  }
}
